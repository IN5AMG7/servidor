﻿Imports System.ComponentModel
Imports Ciber_Z1373_Servidor

Public Class ViewModelAgregarClientes
    Implements ICommand, INotifyPropertyChanged
    Private agregarClientes As AgregarClientes
    Private _agregarClientesVM As ViewModelAgregarClientes
    Private _nombre As String
    Private _nit As String

    Public Property VCAgregarClientes
        Get
            Return _agregarClientesVM
        End Get
        Set(value)
            _agregarClientesVM = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
            NotificarCambio(Nombre)
        End Set
    End Property

    Public Property Nit As String
        Get
            Return _nit
        End Get
        Set(value As String)
            _nit = value
            NotificarCambio(Nit)
        End Set
    End Property

    Public Sub New(agregarClientes As AgregarClientes)
        Me.agregarClientes = agregarClientes
        Me.VCAgregarClientes = Me
        SQLConnectionDataBase.MostrarClientes(agregarClientes.DatosClientes)
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propiedad))
    End Sub

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter
            Case "Agregar"
                AgregarCliente()
        End Select

    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub AgregarCliente()
        SQLConnectionDataBase.AgregarClientes(New Clientes() With
            {.nombre = Nombre,
            .nit = Nit}
         )
        Nombre = ""
        Nit = ""
        MsgBox("El cliente ha sido registrado! :D", MsgBoxStyle.Information, "Gestión de Clientes")
        SQLConnectionDataBase.MostrarClientes(agregarClientes.DatosClientes)
    End Sub
End Class
