﻿Imports System.ComponentModel
Imports Ciber_Z1373_Servidor

Public Class ViewModelConfirmacion
    Implements ICommand, INotifyPropertyChanged

    Private confirmacion As Confirmacion
    Private _vcConfirmacion As ViewModelConfirmacion
    Shared _conexion As New CiberZ1373Entities()

    Public Property VCViewModelConfirmacion
        Get
            Return _vcConfirmacion
        End Get
        Set(value)
            _vcConfirmacion = value
        End Set
    End Property

    Public Sub New(confirmacion As Confirmacion)
        Me.confirmacion = confirmacion
        Me.VCViewModelConfirmacion = Me
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter
            Case "Aceptar"
                Confirmar(confirmacion.Clave.Password)
            Case "Cancelar"
                confirmacion.Close()
        End Select
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Function Confirmar(ByVal clave As String) As Boolean
        Dim _acceso As Boolean = False
        Dim usuario As String = "admin"
        Dim query = (From a In _conexion.Usuarios Where a.nombre = usuario And a.clave = clave).SingleOrDefault
        _acceso = False
        Try
            With query
                .clave = clave
                _acceso = True
            End With
            confirmacion.Hide()
        Catch ex As Exception
            _acceso = False
            MsgBox("Credenciales incorrectas")
            confirmacion.Show()
        End Try
        Return Confirmar
    End Function
End Class
