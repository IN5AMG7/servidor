﻿Imports System.ComponentModel
Imports Ciber_Z1373_Servidor

Public Class ViewModelResumenes
    Implements ICommand, INotifyPropertyChanged

    Private resumenes As Resumenes
    Private _vcResumenes As ViewModelResumenes

    Public Property VCViewModelResumenes
        Get
            Return _vcResumenes
        End Get
        Set(value)
            _vcResumenes = value
        End Set
    End Property

    Public Sub New(resumenes As Resumenes)
        Me.VCViewModelResumenes = Me
        Me.resumenes = resumenes
        MostrarDatos()
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propiedad))
    End Sub


    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Throw New NotImplementedException()
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub MostrarDatos()
        SQLConnectionDataBase.MostrarVista(resumenes.DatosDetalleFactura)
    End Sub
End Class
