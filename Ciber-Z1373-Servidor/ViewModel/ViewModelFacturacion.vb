﻿Imports System.ComponentModel
Imports Ciber_Z1373_Servidor

Public Class ViewModelFacturacion
    Implements ICommand, INotifyPropertyChanged

    Private facturacion As Facturacion
    Private _vcFacturacion As ViewModelFacturacion
    Private _fechaHora As String
    Private _seleccionarCliente As Object
    Private _seleccionarSocio As Object
    Private _idFactura As Object
    Private _seleccionarComputadora As Object
    Private _seleccionarTiempo As Object
    Private _seleccionarArticulo As Object
    Private _total As Double
    Private _iva As Double
    Private _pagar As Double
    Private _vuelto As Double
    Private _nombre As String
    Private _precio As Double
    Private _descripcion As String

#Region "Propiedades de Facturacion"
    Public Property VCViewModelFacturacion
        Get
            Return _vcFacturacion
        End Get
        Set(value)
            _vcFacturacion = value
        End Set
    End Property

    Public Property SeleccionarSocio As Object
        Get
            Return _seleccionarSocio
        End Get
        Set(value As Object)
            _seleccionarSocio = value
        End Set
    End Property

    Public Property SeleccionarCliente As Object
        Get
            Return _seleccionarCliente
        End Get
        Set(value As Object)
            _seleccionarCliente = value
        End Set
    End Property

    Public Property SeleccionarFactura As Object
        Get
            Return _idFactura
        End Get
        Set(value As Object)
            _idFactura = value
        End Set
    End Property

    Public Property FechaHora As String
        Get
            Return _fechaHora
        End Get
        Set(value As String)
            _fechaHora = value
            NotificarCambio(FechaHora)
        End Set
    End Property

    Public Property SeleccionarComputadora As Object
        Get
            Return _seleccionarComputadora
        End Get
        Set(value As Object)
            _seleccionarComputadora = value
        End Set
    End Property

    Public Property SeleccionarArticulo As Object
        Get
            Return _seleccionarArticulo
        End Get
        Set(value As Object)
            _seleccionarArticulo = value
        End Set
    End Property

    Public Property SeleccionarTiempo As Object
        Get
            Return _seleccionarTiempo
        End Get
        Set(value As Object)
            _seleccionarTiempo = value
        End Set
    End Property

    Public Property Total As Double
        Get
            Return _total
        End Get
        Set(value As Double)
            _total = value
            NotificarCambio(Total)
        End Set
    End Property

    Public Property Iva As Double
        Get
            Return _iva
        End Get
        Set(value As Double)
            _iva = value
            NotificarCambio(Iva)
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property
    Public Property Precio As Double
        Get
            Return _precio
        End Get
        Set(value As Double)
            _precio = value
        End Set
    End Property

    Public Property Pagar As Double
        Get
            Return _pagar
        End Get
        Set(value As Double)
            _pagar = value
        End Set
    End Property

    Public Property Vuelto As Double
        Get
            Return _vuelto
        End Get
        Set(value As Double)
            _vuelto = value
        End Set
    End Property
#End Region

    Public Sub New(facturacion As Facturacion)
        Me.facturacion = facturacion
        Me.VCViewModelFacturacion = Me
        FechaHora = DateTime.Now.Day.ToString & "/" & DateTime.Now.Month.ToString & "/" & DateTime.Now.Year.ToString & " " & DateTime.Now.Hour.ToString() & ":" & DateTime.Now.Minute.ToString() & ":" & DateTime.Now.Second.ToString()
        SQLConnectionDataBase.CargarComBoxSocio(facturacion.DatosSocios)
        SQLConnectionDataBase.CargarComBoxCliente(facturacion.DatosClientes)
        facturacion.Principal.Children.Remove(facturacion.Articulos)
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propiedad))
    End Sub

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter
            Case "AgregarCliente"
                AgregarCliente()
            Case "GenerarFactura"
                AgregarFactura()
            Case "AgregarVenta"
                AgregarDetalleFactura()
            Case "Cobrar"
                CobrarFactura()
        End Select
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub AgregarCliente()
        Dim cliente As New AgregarClientes
        cliente.Show()
    End Sub

    Public Sub AgregarFactura()
        Dim idSocio As Integer = CInt(SeleccionarSocio.ToString)
        Dim idCiente As Integer = CInt(SeleccionarCliente.ToString)
        SQLConnectionDataBase.AgregarFactura(New Factura() With
            {.fecha = FechaHora,
            .idSocio = idSocio,
            .idCliente = idCiente,
            .iva = Iva,
            .total = Total}
         )
        facturacion.GenerarFactura.IsEnabled = False
        MostrarDetalleFactura()
    End Sub

    Public Sub MostrarDetalleFactura()
        facturacion.Principal.Children.Add(facturacion.Articulos)
        SQLConnectionDataBase.CargarComBoxArticulos(facturacion.DatosArticulos)
        SQLConnectionDataBase.CargarComBoxComputadoras(facturacion.DatosCompus)
        SQLConnectionDataBase.CargarComBoxHoras(facturacion.DatosHora)
        SQLConnectionDataBase.CargarComBoxFactura(facturacion.DatosFactura)
    End Sub

    Public Sub AgregarDetalleFactura()
        Dim idComputadora As Integer = CInt(SeleccionarComputadora.ToString)
        Dim idTiempo As Integer = CInt(SeleccionarTiempo.ToString)
        Dim idArticulo As Integer = CInt(SeleccionarArticulo.ToString)
        Dim idFactura As Integer = CInt(SeleccionarFactura.ToString)
        Dim _x As Double = 0
        Dim _y As Double = 0
        Dim _z As Double

        Dim articulo As Articulos = SQLConnectionDataBase.BuscarArticulos(idArticulo)
        Nombre = articulo.nombre.ToString
        Descripcion = articulo.Tipo.nombre.ToString
        Precio = articulo.precio.ToString

        _x = CDbl(articulo.precio.ToString)

        If Nombre = "No Aplica" Then

        Else
            Dim _datos As New Test With {.Nombre = Nombre,
            .Descripcion = Descripcion,
            .Precio = Precio}
            facturacion.DatosCompra.Items.Add(_datos)
        End If
        Dim hora As Horas = SQLConnectionDataBase.BuscarHora(idTiempo)
        Nombre = hora.tiempo.ToString
        Descripcion = "Alquiler de Computadora"
        Precio = hora.precio.ToString

        _y = CDbl(hora.precio.ToString)

        If Nombre = "00:00:00" Then

        Else
            Dim _datos2 As New Test With {.Nombre = Nombre,
            .Descripcion = Descripcion,
            .Precio = Precio}
            facturacion.DatosCompra.Items.Add(_datos2)
        End If

        _z = facturacion.Total.Text + (_x + _y)
        facturacion.Total.Text = _z

        facturacion.CalcularIva.Text = (facturacion.Total.Text * 0.12)

        facturacion.Vuelto.Text = (facturacion.Pagar.Text - facturacion.Total.Text)

        SQLConnectionDataBase.AgregarDetalleFactura(New DetalleFactura() With
                   {.idComputadora = idComputadora,
                .idFactura = idFactura,
                .idHora = idTiempo,
                .idArticulo = idArticulo})
    End Sub

    Public Sub CobrarFactura()
        Dim idFactura As Integer = CInt(SeleccionarFactura.ToString)
        SQLConnectionDataBase.ModificarFactura(idFactura,
                                               facturacion.Total.Text,
                                               facturacion.CalcularIva.Text)
        facturacion.Close()
        Reporte()
    End Sub

    Public Sub Reporte()
        Dim _report As New Report
        _report.Show()
    End Sub

End Class

Public Class Test
    Private _nombre As String
    Private _precio As Double
    Private _descripcion As String

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property
    Public Property Precio As Double
        Get
            Return _precio
        End Get
        Set(value As Double)
            _precio = value
        End Set
    End Property

    Public Shared Widening Operator CType(v As Test) As String
        Throw New NotImplementedException()
    End Operator
End Class
