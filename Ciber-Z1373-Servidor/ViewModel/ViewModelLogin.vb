﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports Ciber_Z1373_Servidor

Public Class ViewModelLogin
    Implements ICommand, INotifyPropertyChanged

#Region "Variables de ViewModelLogin"
    Private _viewModelLogin As ViewModelLogin
    Private _mainWindow As New MainWindow()
    Private _login As Login
    Private _access As Boolean = False
    Private _textUser As String
    Private _textPassword As String
    Shared _connection As New CiberZ1373Entities()
#End Region

#Region "Propiedades de ViewModelLogin"
    Public Property User As String
        Get
            Return _textUser
        End Get
        Set(value As String)
            _textUser = value
            NotificarCambio("User")
        End Set
    End Property

    Public Property Password As String
        Get
            Return _textPassword
        End Get
        Set(value As String)
            _textPassword = value
            NotificarCambio("Password")
        End Set
    End Property

    Public Property VCViewModelLogin
        Get
            Return _viewModelLogin
        End Get
        Set(value)
            _viewModelLogin = value
        End Set
    End Property
#End Region

#Region "Constructor"
    Public Sub New(ByRef _instance As Login)
        Me.VCViewModelLogin = Me
        Me._login = _instance
    End Sub

    Public Sub New()
    End Sub
#End Region

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propiedad))
    End Sub

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter.ToString
            Case "Ingresar"
                AbrirServidor(User, _login.Clave.Password)
        End Select
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub AbrirServidor(ByVal user As String, ByVal password As String)
        Autenticate(user, password)
    End Sub

    Public Function Autenticate(ByVal user As String, ByVal password As String) As Boolean
        Dim query = (From a In _connection.Usuarios Where a.nombre = user And a.clave = password).SingleOrDefault
        _access = False
        Try
            With query
                .nombre = user
                .clave = password
                _access = True
            End With
            _mainWindow.Show()
            _login.Hide()
        Catch ex As Exception
            MsgBox("Credenciales incorrectas")
            _login.Show()
        End Try
        Return Autenticate
    End Function

    Public Function Desautenticate() As Boolean
        Dim _log As New Login
        _log.Show()
        Return False
    End Function

End Class
