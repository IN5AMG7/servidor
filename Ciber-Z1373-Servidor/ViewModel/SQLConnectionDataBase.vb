﻿Public Class SQLConnectionDataBase
    Shared _connection As New CiberZ1373Entities()

#Region "CRUD de Socios desde la base de datos"
    Shared Sub MostrarSocios(ByVal grid As DataGrid)
        Dim query = (From x In _connection.Socios
                     Select x).ToList
        grid.ItemsSource = query
    End Sub

    Shared Sub AgregarSocio(ByRef socio As Socios)
        _connection.Socios.Add(socio)
        _connection.SaveChanges()
    End Sub

    Shared Sub EliminarSocio(ByVal idSocio As Integer)
        Dim query = (From x In _connection.Socios
                     Where x.idSocio = idSocio
                     Select x).SingleOrDefault
        _connection.Socios.Remove(query)
        _connection.SaveChanges()
    End Sub

    Shared Function BuscarSocio(ByVal idSocio As Integer) As Socios
        Dim query = (From x In _connection.Socios
                     Where x.idSocio = idSocio
                     Select x).SingleOrDefault
        Return query
    End Function

    Shared Sub ModificarSocio(ByVal idSocio As Integer, ByVal usuario As String, ByVal clave As String,
                              ByVal nombre As String, ByVal apellido As String, ByVal dpi As Integer,
                              ByVal fecha As Date, ByVal telefono As Integer, ByVal direccion As String,
                              ByVal saldo As Integer)
        Dim query = (From x In _connection.Socios Where x.idSocio = idSocio Select x).SingleOrDefault
        With query
            .usuario = usuario
            .clave = clave
            .nombre = nombre
            .apellido = apellido
            .dpi = dpi
            .fechadeNacimiento = fecha
            .telefono = telefono
            .direccion = direccion
            .saldoInicial = saldo
        End With
        _connection.SaveChanges()
    End Sub
#End Region

#Region "CRUD de Usuarios desde la base de datos"
    Shared Sub MostrarUsuarios(ByVal grid As DataGrid)
        Dim query = (From x In _connection.Usuarios
                     Select x).ToList
        grid.ItemsSource = query
    End Sub

    Shared Sub AgregarUsuario(ByRef usuario As Usuarios)
        _connection.Usuarios.Add(usuario)
        _connection.SaveChanges()
    End Sub

    Shared Sub EliminarUsuario(ByVal idUsuario As Integer)
        Dim query = (From x In _connection.Usuarios
                     Where x.idUsuario = idUsuario
                     Select x).SingleOrDefault
        _connection.Usuarios.Remove(query)
        _connection.SaveChanges()
    End Sub

    Shared Function BuscarUsuario(ByVal idUsuario As Integer) As Usuarios
        Dim query = (From x In _connection.Usuarios
                     Where x.idUsuario = idUsuario
                     Select x).SingleOrDefault
        Return query
    End Function

    Shared Sub ModificarUsuario(ByVal idUsuario As Integer, ByVal usuario As String, ByVal clave As String)
        Dim query = (From x In _connection.Usuarios Where x.idUsuario = idUsuario Select x).SingleOrDefault
        With query
            .nombre = usuario
            .clave = clave
        End With
        _connection.SaveChanges()
    End Sub
#End Region

#Region "CRUD de Articulos desde la base de datos"
    Shared Sub MostrarArticulos(ByVal grid As DataGrid)
        Dim query = (From x In _connection.Articulos
                     Select x).ToList
        grid.ItemsSource = query
    End Sub

    Shared Sub CargarComBoxTipo(ByVal comboBox As ComboBox)
        Dim query = (From x In _connection.Tipo
                     Select x).ToList
        comboBox.ItemsSource = query
        comboBox.DisplayMemberPath = "nombre"
        comboBox.SelectedValuePath = "idTipo"
        comboBox.SelectedValue = -1
    End Sub

    Shared Sub AgregarArticulo(ByRef articulo As Articulos)
        _connection.Articulos.Add(articulo)
        _connection.SaveChanges()
    End Sub

    Shared Sub EliminarArticulo(ByVal idArticulo As Integer)
        Dim query = (From x In _connection.Articulos
                     Where x.idArticulo = idArticulo
                     Select x).SingleOrDefault
        _connection.Articulos.Remove(query)
        _connection.SaveChanges()
    End Sub

    Shared Function BuscarArticulos(ByVal idArticulo As Integer) As Articulos
        Dim query = (From x In _connection.Articulos
                     Where x.idArticulo = idArticulo
                     Select x).SingleOrDefault
        Return query
    End Function

    Shared Sub ModificarArticulo(ByVal idArticulo As Integer, ByVal nombre As String, ByVal codigo As String,
                                 ByVal tipo As Integer, ByVal precio As Integer, ByVal cantidad As String)
        Dim query = (From x In _connection.Articulos Where x.idArticulo = idArticulo Select x).SingleOrDefault
        With query
            .nombre = nombre
            .codigo = codigo
            .idTipo = tipo
            .precio = precio
            .cantidad = cantidad
        End With
        _connection.SaveChanges()
    End Sub
#End Region

#Region "CRUD de Computadoras desde la base de datos"
    Shared Sub MostrarComputadoras(ByVal grid As DataGrid)
        Dim query = (From x In _connection.Computadoras
                     Select x).ToList
        grid.ItemsSource = query
    End Sub

    Shared Sub AgregarComputadora(ByRef computadora As Computadoras)
        _connection.Computadoras.Add(computadora)
        _connection.SaveChanges()
    End Sub

    Shared Sub EliminarComputadora(ByVal idComputadora As Integer)
        Dim query = (From x In _connection.Computadoras
                     Where x.idComputadora = idComputadora
                     Select x).SingleOrDefault
        _connection.Computadoras.Remove(query)
        _connection.SaveChanges()
    End Sub

    Shared Function BuscarComputadora(ByVal idComputadora As Integer) As Computadoras
        Dim query = (From x In _connection.Computadoras
                     Where x.idComputadora = idComputadora
                     Select x).SingleOrDefault
        Return query
    End Function

    Shared Sub ModificarComputadora(ByVal idComputadora As Integer, ByVal nombre As String, ByVal ip As String, ByVal activo As Boolean)
        Dim query = (From x In _connection.Computadoras Where x.idComputadora = idComputadora Select x).SingleOrDefault
        With query
            .nombre = nombre
            .ip = ip
            .activo = activo
        End With
        _connection.SaveChanges()
    End Sub
#End Region

#Region "CR de Clientes desde la base de datos"
    Shared Sub MostrarClientes(ByVal grid As DataGrid)
        Dim query = (From x In _connection.Clientes
                     Select x).ToList
        grid.ItemsSource = query
    End Sub

    Shared Sub AgregarClientes(ByRef cliente As Clientes)
        _connection.Clientes.Add(cliente)
        _connection.SaveChanges()
    End Sub
#End Region

#Region "CR de Facturas desde la base de datos"
    Shared Sub AgregarFactura(ByVal factura As Factura)
        _connection.Factura.Add(factura)
        _connection.SaveChanges()
    End Sub

    Shared Sub CargarComBoxCliente(ByVal comboBox As ComboBox)
        Dim query = (From x In _connection.Clientes
                     Select x).ToList
        comboBox.ItemsSource = query
        comboBox.DisplayMemberPath = "nombre"
        comboBox.SelectedValuePath = "idCliente"
        comboBox.SelectedValue = -1
    End Sub

    Shared Sub CargarComBoxSocio(ByVal comboBox As ComboBox)
        Dim query = (From x In _connection.Socios
                     Select x).ToList
        comboBox.ItemsSource = query
        comboBox.DisplayMemberPath = "nombre"
        comboBox.SelectedValuePath = "idSocio"
        comboBox.SelectedValue = -1
    End Sub

    Shared Sub CargarComBoxFactura(ByVal comboBox As ComboBox)
        Dim query = (From x In _connection.Factura
                     Select x).ToList
        comboBox.ItemsSource = query
        comboBox.SelectedValuePath = "idFactura"
        comboBox.SelectedValue = -1
    End Sub

    Shared Sub CargarComBoxComputadoras(ByVal comboBox As ComboBox)
        Dim query = (From x In _connection.Computadoras
                     Select x).ToList
        comboBox.ItemsSource = query
        comboBox.DisplayMemberPath = "nombre"
        comboBox.SelectedValuePath = "idComputadora"
        comboBox.SelectedValue = -1
    End Sub

    Shared Sub CargarComBoxArticulos(ByVal comboBox As ComboBox)
        Dim query = (From x In _connection.Articulos
                     Select x).ToList
        comboBox.ItemsSource = query
        comboBox.DisplayMemberPath = "nombre"
        comboBox.SelectedValuePath = "idArticulo"
        comboBox.SelectedValue = -1
    End Sub

    Shared Sub CargarComBoxHoras(ByVal comboBox As ComboBox)
        Dim query = (From x In _connection.Horas
                     Select x).ToList
        comboBox.ItemsSource = query
        comboBox.DisplayMemberPath = "tiempo"
        comboBox.SelectedValuePath = "idHora"
        comboBox.SelectedValue = -1
    End Sub

    Shared Sub ModificarFactura(ByVal idFactura As Integer, ByVal total As String, ByVal iva As Double)
        Dim query = (From x In _connection.Factura Where x.idFactura = idFactura Select x).SingleOrDefault
        With query
            .total = total
            .iva = iva
        End With
        _connection.SaveChanges()
    End Sub

    Shared Sub AgregarHorasTabla(ByVal idHora As Integer)
        Dim _grid As New Facturacion
        Dim query = (From x In _connection.Horas Where x.idHora = idHora
                     Select x).ToList
        _grid.DatosCompra.ItemsSource = query
    End Sub

    Shared Sub AgregarArticuloTabla(ByVal idArticulo As Integer, ByVal _grid As DataGrid)
        Dim query = (From x In _connection.Articulos Where x.idArticulo = idArticulo
                     Select x).ToList
        _grid.ItemsSource = query
    End Sub

    Shared Sub AgregarDetalleFactura(ByRef detalleFactura As DetalleFactura)
        _connection.DetalleFactura.Add(detalleFactura)
        _connection.SaveChanges()
    End Sub

    Shared Function BuscarHora(ByVal idHora As Integer) As Horas
        Dim query = (From x In _connection.Horas
                     Where x.idHora = idHora
                     Select x).SingleOrDefault
        Return query
    End Function
#End Region

#Region "Mostrar Vista en tabla"
    Shared Sub MostrarVista(ByVal grid As DataGrid)
        Dim query = (From x In _connection.VIEW_Facturas).ToList
        grid.ItemsSource = query
    End Sub
#End Region

#Region "CRUD de Cupones desde la base de datos"
    Shared Sub MostrarCupones(ByVal grid As DataGrid)
        Dim query = (From x In _connection.Cupones
                     Select x).ToList
        grid.ItemsSource = query
    End Sub

    Shared Sub AgregarCupones(ByRef cupon As Cupones)
        _connection.Cupones.Add(cupon)
        _connection.SaveChanges()
    End Sub

    Shared Sub EliminarCupon(ByVal idCupon As Integer)
        Dim query = (From x In _connection.Cupones
                     Where x.idCupones = idCupon
                     Select x).SingleOrDefault
        _connection.Cupones.Remove(query)
        _connection.SaveChanges()
    End Sub

    Shared Function BuscarCupon(ByVal idCupon As Integer) As Cupones
        Dim query = (From x In _connection.Cupones
                     Where x.idCupones = idCupon
                     Select x).SingleOrDefault
        Return query
    End Function

    Shared Sub ModificarCupon(ByVal idCupon As Integer, ByVal nombre As String, ByVal clave As String, ByVal activo As Boolean)
        Dim query = (From x In _connection.Cupones Where x.idCupones = idCupon Select x).SingleOrDefault
        With query
            .usuario = nombre
            .clave = clave
            .estado = activo
        End With
        _connection.SaveChanges()
    End Sub
#End Region

End Class
