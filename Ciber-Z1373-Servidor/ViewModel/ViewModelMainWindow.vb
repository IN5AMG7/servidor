﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Diagnostics

Imports Ciber_Z1373_Servidor

Public Class ViewModelMainWindow
    Implements ICommand, INotifyPropertyChanged

#Region "Variables declaradas en la clase"
    Private _mainWindow As MainWindow
    Private _viewModelMainWindow As ViewModelMainWindow
    Shared _connection As New CiberZ1373Entities()
    Private _idSocio As Integer
    Private _usuario As String = ""
    Private _clave As String = ""
    Private _nombre As String = ""
    Private _apellido As String = ""
    Private _dpi As Integer
    Private _fechaNacimiento As Date
    Private _telefono As Integer
    Private _direccion As String = ""
    Private _saldoInicial As Integer
    Private _codigo As String
    Private _tipo As Integer
    Private _cantidad As String
    Private _precio As Double
    Private _ip As String
    Private _activo As Boolean
    Public _seleccionarSocio As New Object
    Public _seleccionarUsuario As New Object
    Public _seleccionarTipo As New Object
    Public _seleccionarArticulo As New Object
    Public _seleccionarComputadora As New Object
    Public _seleccionarCupon As Object
#End Region

#Region "Constructor de la clase ViewModelMainWindow"
    Public Sub New(ByVal _instance As MainWindow)
        Me.VCViewModelMainWindow = Me
        Me._mainWindow = _instance
        _mainWindow.Width = SystemParameters.PrimaryScreenWidth * 0.7
        _mainWindow.Height = SystemParameters.PrimaryScreenHeight * 0.8

        MostrarComputadoras()
    End Sub
#End Region

#Region "Propiedades de la clase ViewModelMainWindow"
    Public Property VCViewModelMainWindow
        Get
            Return _viewModelMainWindow
        End Get
        Set(value)
            _viewModelMainWindow = value
        End Set
    End Property

    Public Property SeleccionarCupon As Object
        Get
            Return _seleccionarCupon
        End Get
        Set(value As Object)
            _seleccionarCupon = value
        End Set
    End Property

    Public Property IdSocio As Integer
        Get
            Return _idSocio
        End Get
        Set(value As Integer)
            _idSocio = value
            NotificarCambio(IdSocio)
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return _usuario
        End Get
        Set(value As String)
            _usuario = value
            NotificarCambio(Usuario)
        End Set
    End Property

    Public Property Clave As String
        Get
            Return _clave
        End Get
        Set(value As String)
            _clave = value
            NotificarCambio(Clave)
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
            NotificarCambio(Nombre)
        End Set
    End Property

    Public Property Apellido As String
        Get
            Return _apellido
        End Get
        Set(value As String)
            _apellido = value
            NotificarCambio(Apellido)
        End Set
    End Property

    Public Property Dpi As Integer
        Get
            Return _dpi
        End Get
        Set(value As Integer)
            _dpi = value
            NotificarCambio(Dpi)
        End Set
    End Property

    Public Property FechaNacimiento As Date
        Get
            Return _fechaNacimiento
        End Get
        Set(value As Date)
            _fechaNacimiento = value
            NotificarCambio(FechaNacimiento)
        End Set
    End Property

    Public Property Telefono As Integer
        Get
            Return _telefono
        End Get
        Set(value As Integer)
            _telefono = value
            NotificarCambio(Telefono)
        End Set
    End Property

    Public Property Direccion As String
        Get
            Return _direccion
        End Get
        Set(value As String)
            _direccion = value
            NotificarCambio(Direccion)
        End Set
    End Property

    Public Property Saldo As Integer
        Get
            Return _saldoInicial
        End Get
        Set(value As Integer)
            _saldoInicial = value
            NotificarCambio(Saldo)
        End Set
    End Property

    Public Property Tipo As Integer
        Get
            Return _tipo
        End Get
        Set(value As Integer)
            _tipo = value
            NotificarCambio(Tipo)
        End Set
    End Property

    Public Property Codigo As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
            NotificarCambio(Codigo)
        End Set
    End Property

    Public Property Cantidad As String
        Get
            Return _cantidad
        End Get
        Set(value As String)
            _cantidad = value
            NotificarCambio(Cantidad)
        End Set
    End Property

    Public Property Precio As Double
        Get
            Return _precio
        End Get
        Set(value As Double)
            _precio = value
            NotificarCambio(Precio)
        End Set
    End Property

    Public Property Ip As String
        Get
            Return _ip
        End Get
        Set(value As String)
            _ip = value
            NotificarCambio(Ip)
        End Set
    End Property

    Public Property Activo As Boolean
        Get
            Return _activo
        End Get
        Set(value As Boolean)
            _activo = value
            NotificarCambio(Activo)
        End Set
    End Property

    Public Property SeleccionarSocio As Object
        Get
            Return _seleccionarSocio
        End Get
        Set(value As Object)
            _seleccionarSocio = value
        End Set
    End Property

    Public Property SeleccionarUsuario As Object
        Get
            Return _seleccionarUsuario
        End Get
        Set(value As Object)
            _seleccionarUsuario = value
        End Set
    End Property

    Public Property SeleccionarTipo As Object
        Get
            Return _seleccionarTipo
        End Get
        Set(value As Object)
            _seleccionarTipo = value
        End Set
    End Property

    Public Property SeleccionarArticulo As Object
        Get
            Return _seleccionarArticulo
        End Get
       Set (value As Object)
            _seleccionarArticulo = value
        End Set
    End Property

    Public Property SeleccionarComputadora As Object
        Get
            Return _seleccionarComputadora
        End Get
        Set(value As Object)
            _seleccionarComputadora = value
        End Set
    End Property
#End Region

#Region "Interfaz INotifyPropertyChanged / NotificarCambio"
    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propiedad))
    End Sub
#End Region

#Region "Interfaz ICommand"
    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter.ToString
            'Accionar Botones para Navegar en el Servidor
            Case "Computadoras"
                MostrarComputadoras()
            Case "Usuarios"
                MostrarUsuarios()
            Case "Tickets"
                MostrarTickets()
            Case "Socios"
                MostrarSocios()
            Case "Tienda"
                MostrarTienda()
            Case "Configuracion"
                MostrarConfiguracion()
            Case "CerrarSesion"
                CerrarSesion()
            Case "VentaDirecta"
                VentaDirecta()
            Case "Resumenes"
                Resumenes()
            Case "BorrarTodo"
                MostrarConfirmacion()
            Case "VerTotal"
                MostrarConfirmacion()
            Case "RetirarEfectivo"
                MostrarConfirmacion()
            Case "IngresarEfectivo"
                MostrarConfirmacion()

            'Ventana de Usuarios
            Case "ActualizarUsuario"
                SQLConnectionDataBase.MostrarUsuarios(_mainWindow.DatosUsuarios)
            Case "AgregarUsuarios"
                _mainWindow.Usuarios.Children.Add(_mainWindow.AgregarUsuarios)
            Case "AgregarUsuario"
                AgregarUsuario()
                MostrarUsuarios()
            Case "ModificarUsuarios"
                BuscarUsuario()
                _mainWindow.Usuarios.Children.Add(_mainWindow.ModificarUsuarios)
            Case "ModificarUsuario"
                ModificarUsuario()
                MostrarUsuarios()
            Case "EliminarUsuarios"
                EliminarUsuario()
            Case "CancelarUsuario"
                MostrarUsuarios()


            'Ventana de Socios
            Case "Actualizar"
                SQLConnectionDataBase.MostrarSocios(_mainWindow.DatosSocios)
            Case "AgregarSocios"
                _mainWindow.Socios.Children.Add(_mainWindow.Agregar)
            Case "AgregarSocio"
                AgregarSocio()
                MostrarSocios()
            Case "ModificarSocios"
                BuscarSocio()
                _mainWindow.Socios.Children.Add(_mainWindow.Modificar)
            Case "ModificarSocio"
                ModificarSocio()
                MostrarSocios()
            Case "EliminarSocios"
                EliminarSocio()
            Case "Cancelar"
                MostrarSocios()

                'Ventana de Tienda
            Case "AgregarArticulos"
                MostrarTienda()
                _mainWindow.Tienda.Children.Add(_mainWindow.AgregarTienda)
                SQLConnectionDataBase.CargarComBoxTipo(_mainWindow.Tipo)
            Case "AgregarArticulo"
                AgregarArticulo()
                MostrarTienda()
            Case "EliminarArticulos"
                EliminarArticulo()
            Case "CancelarArticulo"
                MostrarTienda()

                'Ventana de Tickets
            Case "AgregarCupones"
                _mainWindow.Cupon.Children.Add(_mainWindow.AgregarCupones)
            Case "ModificarCupones"
                BuscarCupon()
                _mainWindow.Cupon.Children.Add(_mainWindow.ModificarCupones)
            Case "EliminarCupones"
                EliminarCupon()
            Case "CancelarCupon"
                MostrarTickets()
            Case "AgregarCupon"
                AgregarCupon()
                MostrarTickets()
            Case "ModificarCupon"
                ModificarCupon()
                MostrarTickets()
            Case "ActualizarCupon"
                MostrarTickets()

                'Ventana de Computadoras
            Case "AgregarComputadoras"
                _mainWindow.Compus.Children.Add(_mainWindow.AgregarComputadoras)
            Case "AgregarComputadora"
                AgregarComputadora()
            Case "ModificarComputadoras"
                BuscarComputadora()
                _mainWindow.Compus.Children.Add(_mainWindow.ModificarComputadoras)
            Case "ModificarComputadora"
                ModificarComputadora()
                MostrarComputadoras()
            Case "CancelarComputadora"
                MostrarComputadoras()
            Case "EliminarComputadoras"
                EliminarComputadora()
            Case "ActualizarComputadoras"
                SQLConnectionDataBase.MostrarComputadoras(_mainWindow.DatosComputadoras)
            Case "AsignarServicio"
                BuscarComputadora2()
                _mainWindow.Compus.Children.Remove(_mainWindow.PrincipalComputadoraII)
                _mainWindow.Compus.Children.Add(_mainWindow.AsignarServicio)
            Case "ApagarComputadora"
                ApagarPC()
            Case "ReiniciarComputadora"
                ReiniciarPC()
            Case "SuspenderComputadora"
                SuspenderPC()
        End Select
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
#End Region

#Region "Mostrar Ventanas"
    Public Sub MostrarComputadoras()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Menu)
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Compus)
        _mainWindow.Compus.Children.Clear()
        _mainWindow.Compus.Children.Add(_mainWindow.PrincipalComputadoraI)
        _mainWindow.Compus.Children.Add(_mainWindow.PrincipalComputadoraII)
        _mainWindow.Compus.Children.Add(_mainWindow.TablaComputadoras)
        _mainWindow.Compus.Children.Add(_mainWindow.NombreComputadoras)
        SQLConnectionDataBase.MostrarComputadoras(_mainWindow.DatosComputadoras)
    End Sub

    Public Sub MostrarUsuarios()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Menu)
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Usuarios)
        _mainWindow.Usuarios.Children.Clear()
        _mainWindow.Usuarios.Children.Add(_mainWindow.PrincipalUsuarioI)
        _mainWindow.Usuarios.Children.Add(_mainWindow.PrincipalUsuarioII)
        _mainWindow.Usuarios.Children.Add(_mainWindow.TablaUsuarios)
        _mainWindow.Usuarios.Children.Add(_mainWindow.NombreUsuario)
        SQLConnectionDataBase.MostrarUsuarios(_mainWindow.DatosUsuarios)
    End Sub

    Public Sub MostrarTickets()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Menu)
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Cupon)
        _mainWindow.Cupon.Children.Clear()
        _mainWindow.Cupon.Children.Add(_mainWindow.PrincipalCuponesI)
        _mainWindow.Cupon.Children.Add(_mainWindow.PrincipalCuponesII)
        _mainWindow.Cupon.Children.Add(_mainWindow.TablaCupones)
        _mainWindow.Cupon.Children.Add(_mainWindow.NombreCupones)
        SQLConnectionDataBase.MostrarCupones(_mainWindow.DatosCupones)
    End Sub

    Public Sub MostrarSocios()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Menu)
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Socios)
        _mainWindow.Socios.Children.Clear()
        _mainWindow.Socios.Children.Add(_mainWindow.PrincipalSocioI)
        _mainWindow.Socios.Children.Add(_mainWindow.PrincipalSocioII)
        _mainWindow.Socios.Children.Add(_mainWindow.Tabla)
        _mainWindow.Socios.Children.Add(_mainWindow.Nombre)
        SQLConnectionDataBase.MostrarSocios(_mainWindow.DatosSocios)
    End Sub

    Public Sub MostrarTienda()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Menu)
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Tienda)
        _mainWindow.Tienda.Children.Clear()
        _mainWindow.Tienda.Children.Add(_mainWindow.PrincipalTiendaI)
        _mainWindow.Tienda.Children.Add(_mainWindow.PrincipalTiendaII)
        _mainWindow.Tienda.Children.Add(_mainWindow.TablaTienda)
        _mainWindow.Tienda.Children.Add(_mainWindow.NombreTienda)
        SQLConnectionDataBase.MostrarArticulos(_mainWindow.DatosArticulos)
    End Sub

    Public Sub MostrarConfiguracion()
        Dim _configuracion As New Configurar()
        Try
            _configuracion.Show()
        Catch ex As InvalidOperationException
        End Try
    End Sub

    Public Sub CerrarSesion()
        Dim _viewModelLogin As New ViewModelLogin()
        _viewModelLogin.Desautenticate()
        _mainWindow.Close()
    End Sub

    Public Sub VentaDirecta()
        Dim _facturar As New Facturacion
        _facturar.Show()
    End Sub

    Public Sub Resumenes()
        Dim _resumen As New Resumenes
        _resumen.Show()
    End Sub

    Public Sub MostrarConfirmacion()
        Dim _confirmar As New Confirmacion
        _confirmar.Show()
    End Sub
#End Region

#Region "CRUD de Socios"
    Public Sub AgregarSocio()
        SQLConnectionDataBase.AgregarSocio(New Socios() With
            {.usuario = Usuario,
            .clave = Clave,
            .nombre = Nombre,
            .apellido = Apellido,
            .dpi = Dpi,
            .fechadeNacimiento = FechaNacimiento,
            .telefono = Telefono,
            .direccion = Direccion,
            .saldoInicial = Saldo}
         )
        Usuario = ""
        Clave = ""
        Nombre = ""
        Apellido = ""
        Direccion = ""
        MsgBox("El socio ha sido registrado! :D", MsgBoxStyle.Information, "Gestión de Socios")
    End Sub

    Public Sub EliminarSocio()
        If MsgBox("Confirma la eliminacion", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmación") = MsgBoxResult.Yes Then
            Dim idSocio As Integer = CInt(SeleccionarSocio.ToString)
            SQLConnectionDataBase.EliminarSocio(idSocio)
            SQLConnectionDataBase.MostrarSocios(_mainWindow.DatosSocios)
        End If
    End Sub

    Public Sub ModificarSocio()
        Dim idSocio As Integer = CInt(SeleccionarSocio.ToString)
        SQLConnectionDataBase.ModificarSocio(idSocio, Usuario, Clave, Nombre, Apellido, Dpi, FechaNacimiento, Telefono, Direccion, Saldo)
        Usuario = ""
        Clave = ""
        Nombre = ""
        Apellido = ""
        Direccion = ""
        MsgBox("El socio ha sido modificado! :D", MsgBoxStyle.Information, "Gestión de Socios")
    End Sub

    Public Sub BuscarSocio()
        Dim idSocio As Integer = CInt(SeleccionarSocio.ToString)
        Dim socio As Socios = SQLConnectionDataBase.BuscarSocio(idSocio)
        Usuario = socio.usuario
        Clave = socio.clave
        Nombre = socio.nombre
        Apellido = socio.apellido
        Dpi = socio.dpi
        FechaNacimiento = socio.fechadeNacimiento
        Telefono = socio.telefono
        Direccion = socio.direccion
        Saldo = socio.saldoInicial
    End Sub
#End Region

#Region "CRUD de Usuarios"
    Public Sub AgregarUsuario()
        SQLConnectionDataBase.AgregarUsuario(New Usuarios() With
            {.nombre = Usuario,
            .clave = Clave}
         )
        Usuario = ""
        Clave = ""
        MsgBox("El cajero ha sido registrado! :D", MsgBoxStyle.Information, "Gestión de Cajeros")
    End Sub

    Public Sub EliminarUsuario()
        If MsgBox("Confirma la eliminacion", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmación") = MsgBoxResult.Yes Then
            Dim idUsuario As Integer = CInt(SeleccionarUsuario.ToString)
            SQLConnectionDataBase.EliminarUsuario(idUsuario)
            SQLConnectionDataBase.MostrarUsuarios(_mainWindow.DatosUsuarios)
        End If
    End Sub

    Public Sub ModificarUsuario()
        Dim idUsuario As Integer = CInt(SeleccionarUsuario.ToString)
        SQLConnectionDataBase.ModificarUsuario(idUsuario, Usuario, Clave)
        Usuario = ""
        Clave = ""
        MsgBox("El cajero ha sido modificado! :D", MsgBoxStyle.Information, "Gestión de Cajeros")
    End Sub

    Public Sub BuscarUsuario()
        Dim idUsuario As Integer = CInt(SeleccionarUsuario.ToString)
        Dim user As Usuarios = SQLConnectionDataBase.BuscarUsuario(idUsuario)
        Usuario = user.nombre
        Clave = user.clave
    End Sub
#End Region

#Region "CRUD de Articulos"
    Public Sub AgregarArticulo()
        Dim idTipo As Integer = CInt(SeleccionarTipo.ToString)
        SQLConnectionDataBase.AgregarArticulo(New Articulos() With
            {.nombre = Nombre,
            .codigo = Codigo,
            .idTipo = idTipo,
            .cantidad = Cantidad,
            .precio = Precio}
         )
        Nombre = ""
        Codigo = ""
        Cantidad = ""
        MsgBox("El artículo ha sido registrado! :D", MsgBoxStyle.Information, "Gestión de Artículos")
    End Sub

    Public Sub EliminarArticulo()
        If MsgBox("Confirma la eliminacion", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmación") = MsgBoxResult.Yes Then
            Dim idArticulo As Integer = CInt(SeleccionarArticulo.ToString)
            SQLConnectionDataBase.EliminarArticulo(idArticulo)
            SQLConnectionDataBase.MostrarArticulos(_mainWindow.DatosArticulos)
        End If
    End Sub

    Public Sub ModificarArticulo()
        Dim idUsuario As Integer = CInt(SeleccionarUsuario.ToString)
        SQLConnectionDataBase.ModificarUsuario(idUsuario, Usuario, Clave)
        Usuario = ""
        Clave = ""
        MsgBox("El usuario ha sido modificado! :D", MsgBoxStyle.Information, "Gestión de Usuarios")
    End Sub

    Public Sub BuscarArticulo()
        Dim idUsuario As Integer = CInt(SeleccionarUsuario.ToString)
        Dim user As Usuarios = SQLConnectionDataBase.BuscarUsuario(idUsuario)
        Usuario = user.nombre
        Clave = user.clave
    End Sub
#End Region

#Region "CRUD Computadoras"
    Public Sub AgregarComputadora()
        SQLConnectionDataBase.AgregarComputadora(New Computadoras() With
            {.nombre = Nombre,
            .ip = Ip,
            .activo = Activo}
         )
        Nombre = ""
        Ip = ""
        Activo = False
        MsgBox("La computadora ha sido registrada! :D", MsgBoxStyle.Information, "Gestión de Computadoras")
    End Sub

    Public Sub EliminarComputadora()
        If MsgBox("Confirma la eliminación", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmación") = MsgBoxResult.Yes Then
            Dim idComputadora As Integer = CInt(SeleccionarComputadora.ToString)
            SQLConnectionDataBase.EliminarComputadora(idComputadora)
            SQLConnectionDataBase.MostrarComputadoras(_mainWindow.DatosComputadoras)
        End If
    End Sub

    Public Sub ModificarComputadora()
        Dim idComputadora As Integer = CInt(SeleccionarComputadora.ToString)
        Activo = False
        SQLConnectionDataBase.ModificarComputadora(idComputadora, Nombre, Ip, Activo)
        Nombre = ""
        Ip = ""
        MsgBox("La computadora ha sido modificada! :D", MsgBoxStyle.Information, "Gestión de Computadoras")
    End Sub

    Public Sub BuscarComputadora()
        Dim idComputadora As Integer = CInt(SeleccionarComputadora.ToString)
        Dim computadora As Computadoras = SQLConnectionDataBase.BuscarComputadora(idComputadora)
        Nombre = computadora.nombre
        Ip = computadora.ip
        Activo = computadora.activo
    End Sub

    Public Sub BuscarComputadora2()
        Dim idComputadora As Integer = CInt(SeleccionarComputadora.ToString)
        Dim computadora As Computadoras = SQLConnectionDataBase.BuscarComputadora(idComputadora)
        Ip = computadora.ip
        Activo = computadora.activo
    End Sub
#End Region

#Region "CRUD Cupones"
    Public Sub AgregarCupon()
        SQLConnectionDataBase.AgregarCupones(New Cupones() With
            {.usuario = Usuario,
            .clave = Clave,
            .estado = False}
         )
        Usuario = ""
        Clave = ""
        Activo = False
        MsgBox("El cupón ha sido registrado! :D", MsgBoxStyle.Information, "Gestión de Cupones")
    End Sub

    Public Sub EliminarCupon()
        If MsgBox("Confirma la eliminación", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmación") = MsgBoxResult.Yes Then
            Dim idCupon As Integer = CInt(SeleccionarCupon.ToString)
            SQLConnectionDataBase.EliminarCupon(idCupon)
            SQLConnectionDataBase.MostrarCupones(_mainWindow.DatosCupones)
        End If
    End Sub

    Public Sub ModificarCupon()
        Dim idCupon As Integer = CInt(SeleccionarCupon.ToString)
        SQLConnectionDataBase.ModificarCupon(idCupon, Usuario, Clave, Activo)
        Nombre = ""
        Ip = ""
        Activo = False
        MsgBox("El cupón ha sido modificado! :D", MsgBoxStyle.Information, "Gestión de Cupones")
    End Sub

    Public Sub BuscarCupon()
        Dim idCupon As Integer = CInt(SeleccionarCupon.ToString)
        Dim cupon As Cupones = SQLConnectionDataBase.BuscarCupon(idCupon)
        Usuario = cupon.usuario
        Clave = cupon.clave
        Activo = cupon.estado
    End Sub
#End Region

#Region "Manejar Computadora"
    Public Sub ApagarPC()
        Process.Start("Shutdown.exe", "Shutdown -m \\ " & _mainWindow.Direccion.Text & " -s -t 15")
    End Sub

    Public Sub ReiniciarPC()
        Process.Start("Shutdown.exe", "Shutdown -m \\ " & _mainWindow.Direccion.Text & " -r -t 15")
    End Sub

    Public Sub SuspenderPC()
        Process.Start("Shutdown.exe", "Shutdown -m \\ " & _mainWindow.Direccion.Text & " sudo pm-suspend -t 15")
    End Sub
#End Region

End Class
